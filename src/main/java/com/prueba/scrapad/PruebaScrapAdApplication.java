package com.prueba.scrapad;

import com.prueba.scrapad.persistence.AdRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = AdRepository.class)
public class PruebaScrapAdApplication {

    public static void main(String[] args) {
        SpringApplication.run(PruebaScrapAdApplication.class, args);
    }

}
