package com.prueba.scrapad.controller;

import com.prueba.scrapad.payload.DetailResponse;
import com.prueba.scrapad.payload.SearchResponse;
import com.prueba.scrapad.service.DetailService;
import com.prueba.scrapad.service.SearchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@Slf4j
public class AdController {

    private SearchService searchService;
    private DetailService detailService;
    @Autowired
    public void setSearchService (SearchService searchService){
        this.searchService= searchService;
    }

    @Autowired
    public void setDetailService (DetailService detailService){
        this.detailService= detailService;
    }

    @GetMapping("/search")
    public ResponseEntity<SearchResponse> search(@RequestParam final String term, @RequestParam final Integer perPage, @RequestParam final Integer nPage){
        log.info("Recibida petición de busqueda con los siguientes parámetros: [term: {}, perPage: {}, nPage:{}]", term, perPage, nPage);
        return this.searchService.search(term, perPage, nPage);
    }

    @GetMapping("/detail")
    public ResponseEntity<DetailResponse> detail(@RequestParam final UUID id){
        log.info("Recibida petición de detalle del anuncio: {}", id );
        return this.detailService.detail(id);

    }




}
