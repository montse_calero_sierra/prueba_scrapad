package com.prueba.scrapad.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface AdRepository extends MongoRepository<Ad, UUID> {

    /**
     * Búsqueda en nombre del anuncio y/o en materiales
     * @param term Término de búsqueda
     * @param pageable Solicitud de paginación
     * @return Una página con objetos {@link Ad}
     */
    @Query("{$or:[{'name':{$regex:?0,$options:'i'}},{'materials.materialName':{$regex:?0,$options:'i'}}]}")
    Page<Ad> findByNameOrMaterialNameLike(String term, Pageable pageable);

    /**
     * Búsqueda en materiales
     * @param materialName Nombre del material que se busca
     * @param pageable Solicitud de paginación
     * @return Una página con objetos {@link Ad}
     */
    @Query("{'materials.materialName':{$regex:?0,$options:'i'}}")
    Page<Ad> findByMaterialNameLike(String materialName, Pageable pageable);

}
