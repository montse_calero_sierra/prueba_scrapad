package com.prueba.scrapad.persistence;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.UUID;

@Data
@Document
public class Ad {

    private UUID id;
    private String name;
    private Integer amount;
    private Double price; //Note: the price is multiplied by 100 to avoid decimals. ej : 150 -> 1,5
    private List<String> materials;

}
