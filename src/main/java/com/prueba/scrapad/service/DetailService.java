package com.prueba.scrapad.service;

import com.prueba.scrapad.payload.DetailResponse;
import com.prueba.scrapad.persistence.Ad;
import com.prueba.scrapad.persistence.AdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DetailService {
    private AdRepository adRepository;

    @Autowired
    public void setAdRepository(AdRepository adRepository){
        this.adRepository= adRepository;
    }

    /**
     * Método que devuelve el detalle de un anuncio y los relacionados
     * @param idAd UUID del anuncio
     * @return Un objeto DetailResponse con el estado HTTP
     */
    public ResponseEntity<DetailResponse> detail (final UUID idAd){
        final DetailResponse response = new DetailResponse();
        final Optional<Ad> adOptional = adRepository.findById(idAd);
        if (adOptional.isEmpty()) return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        Ad ad = adOptional.get();

        Set<Ad> relatedAds = new HashSet<>();
        for (String material: ad.getMaterials()){
            Page<Ad> page= adRepository.findByMaterialNameLike(material, PageRequest.of(0,10));
            relatedAds.addAll(page.getContent());
        }
        relatedAds.remove(ad);
        response.setId(ad.getId());
        response.setName(ad.getName());
        response.setAmount(ad.getAmount());
        response.setPrice(ad.getPrice());
        response.setRelatedAds(new ArrayList<>(relatedAds));

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
