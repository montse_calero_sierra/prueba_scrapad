package com.prueba.scrapad.service;

import com.prueba.scrapad.payload.SearchResponse;
import com.prueba.scrapad.persistence.Ad;
import com.prueba.scrapad.persistence.AdRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class SearchService {
    private AdRepository adRepository;

    @Autowired
    public void setAdRepository(AdRepository adRepository){
        this.adRepository= adRepository;
    }

    /**
     * Método que devuelve los anuncios que correspondan con el término buscado en función del nombre o de los materiales
     * @param term Término de búsqueda
     * @param perPage  Número de elementos máximos por página
     * @param nPage Número de página que queremos consultar
     * @return Un objeto SearchResponse con el estado HTTP
     */
    public ResponseEntity<SearchResponse> search(final String term, final Integer perPage, final Integer nPage){
        final Pageable paging = PageRequest.of(nPage, perPage);
        final Page<Ad> adPage = adRepository.findByNameOrMaterialNameLike(term, paging);
        final SearchResponse response = new SearchResponse();
        response.setAds(adPage.getContent());
        response.setTotal(adPage.getTotalElements());
        response.setCurrent(adPage.getNumber());
        response.setNextPage(Math.min(adPage.getNumber()+1, adPage.getTotalPages()));
        return new ResponseEntity<>(response, response.getAds().isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK);
    }
}
