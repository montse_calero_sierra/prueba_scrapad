package com.prueba.scrapad;

import com.prueba.scrapad.persistence.Ad;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Component
public class DatabaseInitializer {
    private MongoTemplate mongoTemplate;

    @Autowired
    public void setMongoTemplate(MongoTemplate mongoTemplate) {
        this.mongoTemplate = mongoTemplate;
    }

    /**
     * Método que se ejecuta inmediatamente después de la construcción de la clase e inserta los objetos {@link Ad}
     * en la base de datos si éstos o la colección no existen previamente
     * @throws IOException en caso de error al leer el fichero
     */
    @PostConstruct
    public void init() throws IOException {
        if (mongoTemplate.collectionExists(Ad.class) && mongoTemplate.count(new Query(), Ad.class) > 0) {
            return;
        }
        mongoTemplate.insertAll(convertCsvAds());
    }

    /**
     * Método de lectura del fichero CSV y transformación de los registros en objetos {@link Ad}
     * @return Una lista de {@link Ad}
     * @throws IOException en caso de error al leer el fichero
     */
    private List<Ad> convertCsvAds() throws IOException {
        Map<UUID, Ad> adsMap = new HashMap<>();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new ClassPathResource("ad.csv").getInputStream(), StandardCharsets.UTF_8));

        String line;
        boolean firstLine = true;

        while ((line = reader.readLine()) != null) {
            if (firstLine) {
                firstLine = false;
                continue;
            }

            String[] csvRow = line.split(",");
            UUID idAd = UUID.fromString(csvRow[0]);

            Ad ad = adsMap.getOrDefault(idAd, new Ad());
            ad.setId(idAd);
            ad.setName(csvRow[1]);
            ad.setAmount(Integer.parseInt(csvRow[2]));
            ad.setPrice(Double.parseDouble(csvRow[3]) / 100);

            if(ad.getMaterials() == null){
                ad.setMaterials(new ArrayList<>());
            }
            ad.getMaterials().add(csvRow[4]);

            adsMap.put(idAd, ad);
        }

        reader.close();
        return new ArrayList<>(adsMap.values());

    }
}
