package com.prueba.scrapad.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.prueba.scrapad.persistence.Ad;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SearchResponse {
    private List<Ad> ads;
    private Long total;
    private Integer current;
    private Integer nextPage;

}
