package com.prueba.scrapad.payload;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.prueba.scrapad.persistence.Ad;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class DetailResponse {
    private UUID id;
    private String name;
    private Integer amount;
    private Double price;
    private List<Ad> relatedAds;

}
