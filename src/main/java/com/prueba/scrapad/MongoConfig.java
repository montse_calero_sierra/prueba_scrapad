package com.prueba.scrapad;

import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.bson.UuidRepresentation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;

@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {
    @Value("${spring.data.mongodb.database}")
    private String databaseName;

    @Value("${mongo.url}")
    private String mongoUrl;

    @Override
    protected String getDatabaseName() {
        return databaseName;
    }

    /**
     * Creación del cliente de mongo en función de una URL de conexión
     * Configura la representación del UUID
     * @return Cliente de Mongo
     */
    @Override
    public MongoClient mongoClient() {
        return MongoClients.create(MongoClientSettings.builder()
                .applyConnectionString(new com.mongodb.ConnectionString(mongoUrl))
                .uuidRepresentation(UuidRepresentation.STANDARD)
                .build()
        );
    }

    /**
     * Bean de configuración de MongoTemplate con el cliente de mongo previamente creado y el nombre de la base de datos
     * @return Objeto MongoTemplate
     */
    @Bean
    public MongoTemplate mongoTemplate() {
        return new MongoTemplate(mongoClient(), getDatabaseName());
    }

}
